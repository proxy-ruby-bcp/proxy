require 'em-proxy'

response_data = ""
deffer_response = false

Proxy.start(:host => "127.0.0.1", :port => 8008) do |conn|
  conn.server :srv, :host => "www2.bcp.com.bo", :port => 80

  # modify / process request stream
  conn.on_data do |data|
  
    deffer_response = data["/BCP_Mobile2/BancaMovil.svc/user/products "]

    p "deffer_response #{deffer_response ? "true" : "false"}"

    data
  end

  # modify / process response stream
  conn.on_response do |backend, resp|
    response_data << resp  

    if (deffer_response)
      "{\"messages\":[{\"key\":\"<ErrorKey>\",\"level\":\"<ErrorLevel>\",\"description\":\"<Description>\"}]}"
    else

      if response_data["</HTML>"] || (!response_data["<HTML>"] && response_data.match(/\}$/))
        response_data
      else 
        nil
      end
    end
  end

  # termination logic
  conn.on_finish do |backend, name|
    unbind if backend == :srv
  end

end

